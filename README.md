# React Avanzado: Fullstack Next.js, Apollo, MongoDB y GraphQL

## Lecciones:

* [Introducción al curso](#module01)
    * [Lección #1 - Lo que vamos a construir en este curso](#lesson01)
* [Introducción a GraphQL](#module02)
    * [Lección #2 - ¿Qué es GraphQL?](#lesson02)
    * [Lección #3 - GraphQL y REST](#lesson03)
    * [Lección #4 - GraphQL y SQL](#lesson04)
    * [Lección #5 - GraphQL y Apollo](#lesson05)
    * [Lección #6 - El código final](#lesson06)
* [Primeros pasos con Apollo y GraphQL](#module03)
    * [Lección #7 - Creando un servidor de Apollo](#lesson07)
    * [Lección #8 - Query en GraphQL](#lesson08)
    * [Lección #9 - Mutation](#lesson09)
    * [Lección #10 - Schema](#lesson10)
    * [Lección #11 - Resolvers](#lesson11)
    * [Lección #12 - Agregando Types y Resolvers en el código](#lesson12)
    * [Lección #13 - Agregando un segundo resolver](#lesson13)
    * [Lección #14 - Moviendo los Types y Resolvers hacia su propio archivo](#lesson14)
    * [Lección #15 - Inputs o argumentos para los resolvers](#lesson15)
    * [Lección #16 - Variables en GraphQL](#lesson16)
    * [Lección #17 - El Context en GraphQL](#lesson17)
* [Creando una App Real en GraphQL, Apollo, Next.js, MongoDB y Mongoose](#module04)
    * [Lección #18 - Configurando una base de datos de MongoDB](#lesson18)
    * [Lección #19 - Conectando la base de datos con mongoose](#lesson19)
* [Creación de usuarios](#module05)
    * [Lección #20 - Definiendo el modelo](#lesson20)
    * [Lección #21 - Creando el Type de Usuarios](#lesson21)
    * [Lección #22 - Creando el Input de Usuarios](#lesson22)
    * [Lección #23 - Insertando Usuarios en la base de datos](#lesson23)
    * [Lección #24 - Hasheando los Passwords](#lesson24)
* [Autenticación de usuarios](#module06)
    * [Lección #25 - Creando el Resolver y Mutation](#lesson25)
    * [Lección #26 - Creando un JSON Web Token](#lesson26)
    * [Lección #27 - Obtener el Usuario Autenticado vía JSON Web Token](#lesson27)
* [Creación de productos](#module07)
    * [Lección #28 - Creando el modelo de Producto](#lesson28)
    * [Lección #29 - Creando Type, Input y Resolver en Schema](#lesson29)
    * [Lección #30 - Creando el Resolver para insertar nuevos productos](#lesson30)
* [Queries para obtener los productos y el resto de las acciones del CRUD](#module08)
    * [Lección #31 - Obteniendo todos los productos](#lesson31)
    * [Lección #32 - Obteniendo un producto por su id](#lesson32)
    * [Lección #33 - Actualizar un producto](#lesson33)
    * [Lección #34 - Eliminar un producto](#lesson34)
* [Clientes - Creación, Listado, Actualización y Eliminación](#module09)
    * [Lección #35 - Definiendo el Modelo de Clientes](#lesson35)
    * [Lección #36 - Definiendo Mutation, Input y Type en el Schema](#lesson36)
    * [Lección #37 - Creando el Resolver](#lesson37)
    * [Lección #38 - Asignando Vendedor a Cliente](#lesson38)
    * [Lección #39 - Obtener todos los clientes](#lesson39)
    * [Lección #40 - Obtener los clientes asignados a un vendedor](#lesson40)
    * [Lección #41 - Obtener un Cliente en Específico](#lesson41)
    * [Lección #42 - Actualizar un cliente](#lesson42)
    * [Lección #43 - Eliminar un cliente](#lesson43)
* [Pedidos - Creación por medio de Mutations y Resolvers](#module10)
    * [Lección #44 - Definiendo el Modelo de Pedidos](#lesson44)
    * [Lección #45 - Definiendo el Pedido en el Schema: Inputs, types y Enums](#lesson45)
    * [Lección #46 - Creando el Mutation para nuevos Pedidos](#lesson46)
    * [Lección #47 - Revisar que haya artículos en existencia antes de Crear el Pedido](#lesson47)
    * [Lección #48 - Finalizando el Resolver de creación de Pedidos](#lesson48)
* [Pedidos - Consultar, actualizar y Eliminar](#module11)
    * [Lección #49 - Obtener todos los Pedidos](#lesson49)
    * [Lección #50 - Obtener los Pedidos por Vendedor](#lesson50)
    * [Lección #51 - Obtener un Pedido en Específico](#lesson51)
    * [Lección #52 - Actualizar un Pedido](#lesson52)
    * [Lección #53 - Eliminar un Pedido](#lesson53)
    * [Lección #54 - Obteniendo los Pedidos por su estado](#lesson54)
* [Obtener Mejores Clientes, Mejores Vendedores y búsqueda de Productos](#module12)
    * [Lección #55 - Obteniendo los Mejores Clientes](#lesson55)
    * [Lección #56 - Obteniendo los Mejores Vendedores](#lesson56)
    * [Lección #57 - Buscar Productos por nombre](#lesson57)
* [Apollo Client - Creando un Proyecto con Next.js y React](#module13)
    * [Lección #58 - Creando el Proyecto y SSR de Next.js](#lesson58)
    * [Lección #59 - Modificando el Head del Proyecto y agregando Tailwind](#lesson59)
    * [Lección #60 - Creando el Sidebar del Proyecto](#lesson60)
    * [Lección #61 - Creando el área de contenido principal](#lesson61)
    * [Lección #62 - Routing en Next.js y creando el Menú de Navegación](#lesson62)
    * [Lección #63 - Destacando la página actual en Next.js y Tailwind](#lesson63)
    * [Lección #64 - Mostrar un diseño de forma condicional](#lesson64)
* [Next.js y Apollo - Creando los Formularios de Login y Crear Cuenta](#module14)
    * [Lección #65 - Creando el formulario para Login con Next y Tailwind](#lesson65)
    * [Lección #66 - Creando el Formulario para Crear Nueva Cuenta](#lesson66)
    * [Lección #67 - Añadiendo Formik para el manejo de los formularios](#lesson67)
    * [Lección #68 - Agregando Yup para la validación](#lesson68)
    * [Lección #69 - Agregando el Resto de las validaciones](#lesson69)
    * [Lección #70 - Configurando Apollo Client](#lesson70)
    * [Lección #71 - Instalando y Configurando Apollo Dev Tools](#lesson71)
    * [Lección #72 - Realizando una consulta de prueba para confirmar que todo funciona](#lesson72)
    * [Lección #73 - Creando un Usuario desde el formulario](#lesson73)
    * [Lección #74 - Mostrando un mensaje si el usuario ya está registrado](#lesson74)
    * [Lección #75 - Finalizando la creación de Usuarios](#lesson75)
    * [Lección #76 - Trabajando con el Login y Formik](#lesson76)
    * [Lección #77 - Enviando la petición al servidor con useMutation](#lesson77)
    * [Lección #78 - Almacenando el Token en Storage](#lesson78)

## <a name="module01"></a> Introducción al curso

### <a name="lesson01"></a> Lección #1 - Lo que vamos a construir en este curso
- - -
Creación de una aplicación completa con React, Next.js, GraphQL, Apollo Server, Client, MongoDB, Tailwind y Formik. Será un CRM completo donde pondrá a prueba todas las habilidades adquiridas hasta ahora.

## <a name="module02"></a> Introducción a GraphQL

### <a name="lesson02"></a> Lección #2 - ¿Qué es GraphQL?
- - -
Curiosidades: 
* GraphQL = Graph Query Language
* No es un servidor, no es una base de datos, no es un cliente para realizar consultas.
* Es similar a SQL, ya que es un lenguaje de consultas.
* Para utilizarlo, así como realizar consultas y leer los resultados, deberás tener GraphQL instalado en tu servidor.
* Fué inventado por Facebook como una necesidad para obtener datos del servidor.
* Desarrollado en 2012 pero Open Source desde 2015.
* Independiente de cualquier lenguaje o librería pero muy relacionado con React, aunque también se usa con Angular o Vue.
* Pinterest, Shopify, Github y Coursera utilizan GraphQL hoy en día.

### <a name="lesson03"></a> Lección #3 - GraphQL y REST
- - -
* Las REST API's no tienen nada de malo, pero encontrarás grandes beneficios en GraphQL.
* GraphQL es desarrollado pensando en el performance, en utilizar únicamente los datos que necesitamos.
* Además en GraphQL solo hay una URL.

**El problema de de las REST API's**: Supongamos que quiero traerme todos los clientes, todos los vendedores y todos los productos. Para ello tenemos los siguientes endpoints:
* /clientes
* /vendedores
* /productos

En GraphQL no ocurre, solo haríamos algo de la siguiente manera:

```sh
query {
  clientes {
    nombre
    empresa
    email
  }
}
```

O si quiero en una sola consulta lo de clientes y productos sería de la siguiente forma:

```sh
query {
  clientes {
    nombre
    empresa
    email
  }
  productos {
    nombre
    precio
    existencia
  }
}
```

Entre los beneficios de usar GraphQL podemos encontrar:
* Podemos realizar consultas y mostrar los datos que necesitas.
* No requieres crear nuevos endpoints.
* No importa la versión ya que siempre es compatible con versiones anteriores.
* Se puede instalar sobre una API o base de datos ya existente.
* Es independiente del lenguaje.

### <a name="lesson04"></a> Lección #4 - GraphQL y SQL
- - -
* No hay nada de malo con SQL, es excelente para datos relacionados y para muchas aplicaciones, SQL es la opción adecuada.
* Utilizar GraphQL no es con el fin de dejar de utilizar una base de datos SQL o NoSQL.
* SQL es un lenguaje que ha estado por 40 años y seguramente estará por otros 40 más.
* Incluso puedes colocar GraphQL sobre una base de datos SQL o NoSQL.
* GraphQL nace como una alternativa de SQL para obtener los datos que requieres.
* Fué inventado por Facebook debido a que con SQL era complicado obtener las entradas de las personas, después de sus amigos, de los amigos de sus amigos y de los amigos de los amigos de sus amigos.
* En GraphQL hay separación entre la base de datos y el cliente.
* GraphQL corre sobre base de datos ya sea MongoDB, Redis, MySQL, SQL, PostgreSQL...
* También GraphQL se conecta a cualquier ORM - Mongoose, Sequelize, Doctrine, etc.
* Puedes utilizar GraphQL sobre tu base de datos ya existente.

### <a name="lesson05"></a> Lección #5 - GraphQL y Apollo
- - -
Servidores GraphQL:
* Existen diferentes servidores y servicios que soportan GraphQL.
* Algunas opciones son Apollo Server, GraphQL Yoga o Apollo-server-express. Este último es por si queremos trabajar con el framework express.
* Servicios como Amplify de AWS ya soportan GraphQL. Es muy similar a Firebase.

Apollo es una plataforma que te permitirá trabajar con GraphQL. Viene en dos paquetes:
* Apollo server: Es utilizado para las tareas relacionadas con el servidor y escribir / consultar la base de datos.
* Apollo client: Es utilizado en el cliente (puede ser React) para mostrar los resultados.

Se puede instalar sobre create-react-app o next.js (Apollo client). Nos permite cachear las consultar y datos para mayor performance y tiene métodos para crear aplicaciones en tiempo real.

Para crear un servidor es necesario definir un schema con un type Query como mínimo y un resolver para ese type Query.

### <a name="lesson06"></a> Lección #6 - El código final
- - -
En esta lección tendremos a disposición el código del proyecto. Para ello, debemos descargar los siguientes recursos:
* CRMGraphQL.
* crmcliente.
Si en un momento dado tenemos un problema podemos usar este recurso para comparar nuestro trabajo con el código definitivo.

## <a name="module03"></a> Primeros pasos con Apollo y GraphQL

### <a name="lesson07"></a> Lección #7 - Creando un servidor de Apollo
- - -
Para esta lección, crearemos una carpeta *crm-graphql* en la que accederemos a él y ejecutaremos el comando: ``npm init``. Ahora instalaremos dos dependencias:

```sh
npm i apollo-server --save
npm i -D nodemon --save
```

*Nota: La letra i significa install. El flag -D es instalar una dependencia para desarrollo, es igual a --save-dev.*

En el archivo index.js vamos a poner toda nuestra configuración del servidor de Apollo. En lecciones posteriores definiremos nuestros queries, mutations y resolvers para que funcione apropiadamente.

### <a name="lesson08"></a> Lección #8 - Query en GraphQL
- - -
Los 4 términos importantes en GraphQL son los siguientes:
* Query.
* Mutation.
* Resolver.
* Schema.

Query:
* En un C**R**UD, un query nos permite leer los registros.
* Es la forma de extraer la información existente desde la base de datos o Rest API.
* Es equivalente a un select de SQL o un Get de una Rest API.
* En el query declaras que campos o datos vas a requerir para tu consulta y también soporta parámetros (Input).
* El query en GraphQL es universal, por lo tanto es igual en Angular, Node o React o si la base de datos es NoSQL o SQL.

Ejemplo:
```sh
query {
  obtenerProductos {
    id
    nombre
    precio
    stock
  }
}
```

Con esta consulta tendremos el siguiente resultado:

```sh
{
  "data": {
    "obtenerProductos": [
      {
        "id": "v5645g6fd16b1fg3bn",
        "nombre": "Computadora Laptop",
        "precio": 2000,
        "stock": 1000,
      },
      {
        "id": "1v56sd15v156vb1fd5",
        "nombre": "MacBook",
        "precio": 2000,
        "stock": 10,
      },
    ]
  }
}
```

Pero para lograr esto debimos haber definido un **resolver** que se comunique con la base de datos, etc.

### <a name="lesson09"></a> Lección #9 - Mutation
- - -
Mutation:
* Se utilizar para las otras 3 acciones del **C**R**UD**: Actualizar, Eliminar y Crear Registros.
* Son similares a un PUT / PATCH, DELETE o POST de una Rest API o un DELETE, UPDATE e INSERT de SQL.
* Igual que el **query** son independientes del lenguaje, así que son iguales si tu backend es node, php o python o si tu base de datos es SQL o NoSQL.

Ejemplo de Mutation:
```sh
mutation eliminarProductos ($id: ID) {
  eliminarProducto (id: $id)
}
```

Con esta consulta tendremos el siguiente resultado:

```sh
{
  "data": {
    "eliminarProducto": "Se eliminó correctamente"
  }
}
```

### <a name="lesson10"></a> Lección #10 - Schema
- - -
* Es el que describe tus tipos de objeto, queries y datos de tu aplicación.
* Query es el único que es **obligatorio** en tu schema.
* El schema en GraphQL utiliza un typing en el que le defines si un campo será de tipo string, int, boolean u otro tipo de dato.
* El schema y el resolver están muy relacionados, el schema define la forma de los datos mientras que el resolver se encarga de la comunicación con el lenguaje del servidor y la base de datos.

Ejemplo de Schema:
```sh
type Cliente {
  id: ID
  nombre: String
  apellido: String
  empresa: String
  emails: [Email]
  edad: Int
}
type Email {
  email: String
}
```

Pedidos podemos definir los [tipos de datos que nos ofrece GraphQL](https://graphql.org/learn/schema/) pero también podemos definir nuestros propios tipos. Email es un tipo que estamos definiendo que será un email del tipo String. Los corchetes que podemos ver significa que es un arreglo de emails.

**Importante: Esta estructura debe ser similar a la de tu Base de Datos.**

### <a name="lesson11"></a> Lección #11 - Resolvers
- - -
* Son funciones que son responsables de retornar los valores que existen en tu schema.
* Queries y mutations por si solo no hacen mucho, requieren un backend para realizar las operaciones en la base de datos (y ese backend es el resolver).
* Los nombres de los resolvers **deben ser iguales a los definidos en el schema**.

Ejemplo:
```sh
obtenerClientes: async () => {
  const clientes = await Clientes.find({});

  return clientes;
},
```

Schema y Resolver

En tu Schema de GraphQL debes tener lo siguiente:

```sh
type Query {
  obtenerCliente (id: ID): Cliente
}

type Cliente {
  id: ID
  nombre: String
  apellido: String
  empresa: String
  emails: [Email]
  edad: Int
}

type Email {
  email: String
}
```

En tus resolvers:
```sh
obtenerCliente: async (_, {id}) => {
  // Consultar clientes
  const cliente = await Cliente.findById(id);

  return cliente;
},
```

### <a name="lesson12"></a> Lección #12 - Agregando Types y Resolvers en el código
- - -
Una vez internalizado los conceptos anteriores, definiremos nuestro schema en el código. Si todo se realizó de forma correcta, podemos hacer una consulta de la siguiente manera:

```sh
{
  obtenerCursos {
    titulo
    tecnologia
  }
}
```

El resultado será el siguiente:
```sh
{
  "data": {
    "obtenerCursos": {
      "titulo": "JavaScript Moderno Guía Definitiva Construye +10 Proyectos",
      "tecnologia": "JavaScript ES6"
    }
  }
}
```

### <a name="lesson13"></a> Lección #13 - Agregando un segundo resolver
- - -
En esta lección vamos a crear un tipo, un query y un resolver para obtener tecnología. Una de las ventajas de usar GraphQL es que se encarga de iterar en el arreglo especificado para obtener la propiedad que se le indica en la consulta y por lo tanto no debemos usar un map, un filter, etc.

También podemos hacer dos consultas al mismo tiempo. Ejemplo:
```sh
{
  obtenerCursos {
    titulo
  }
  obtenerTecnologia {
    tecnologia
  }
}
```

Dando como resultado:
```sh
{
  "data": {
    "obtenerCursos": [
      {
        "titulo": "JavaScript Moderno Guía Definitiva Construye +10 Proyectos"
      },
      {
        "titulo": "React – La Guía Completa: Hooks Context Redux MERN +15 Apps"
      },
      {
        "titulo": "Node.js – Bootcamp Desarrollo Web inc. MVC y REST API’s"
      },
      {
        "titulo": "ReactJS Avanzado – FullStack React GraphQL y Apollo"
      }
    ],
    "obtenerTecnologia": [
      {
        "tecnologia": "JavaScript ES6"
      },
      {
        "tecnologia": "React"
      },
      {
        "tecnologia": "Node.js"
      },
      {
        "tecnologia": "React"
      }
    ]
  }
}
```

### <a name="lesson14"></a> Lección #14 - Moviendo los Types y Resolvers hacia su propio archivo
- - -
En esta lección vamos a limpiar un poco el archivo principal del proyecto modularizando nuestro código.

### <a name="lesson15"></a> Lección #15 - Inputs o argumentos para los resolvers
- - -
En esta lección vamos a definir los inputs. Una práctica habitual es nombrarlo como el tipo con el sufijo Input. Ejemplo: Si tenemos un tipo ``Curso`` sería ``CursoInput``.

En el Query, debemos definirlo de la siguiente forma:
```sh
  type Query {
    obtenerCursos(input: CursoInput!): [Curso]
  }
```

*Nota: El signo de exclamación significa que ese parámetro es obligatorio.*

De tal forma, la consulta en GraphQL sería de la siguiente manera:
```sh
{
  obtenerCursos(input: {
    tecnologia: "React"
  }) {
    titulo
  }
}
```

Los resolvers en GraphQL [reciben cuatro argumentos](https://www.apollographql.com/docs/apollo-server/data/resolvers/#resolver-arguments). El primer argumento es un valor que retorna de un resolver padre y es el que nos sirve para hacer consultas anidadas pero eso lo veremos más adelante. El segundo valor son los argumentos que recibe de la consulta. El tercer valor es el contexto y es un valor que se comparte entre todos los resolvers (y es habitualmente usado para la autenticación), se le conoce también como ``ctx`` y el cuarto es la información. En esta lección usaremos los dos primeros argumentos únicamente.

si configuramos todo de forma correcta el resultado sería el siguiente:
```sh
{
  "data": {
    "obtenerCursos": [
      {
        "titulo": "React – La Guía Completa: Hooks Context Redux MERN +15 Apps"
      },
      {
        "titulo": "ReactJS Avanzado – FullStack React GraphQL y Apollo"
      }
    ]
  }
}
```

Otro ejemplo interesante es que pasa si definimos argumentos que no están en el schema, por ejemplo:
```sh
{
  obtenerCursos(input: {
    tecnologia: "Node.js",
    id: 202020
  }) {
    titulo
  }
}
```

El resultado sería el siguiente:
```sh
{
  "error": {
    "errors": [
      {
        "message": "Field \"id\" is not defined by type CursoInput.",
        "locations": [
          {
            "line": 2,
            "column": 48
          }
        ],
        "extensions": {
          "code": "GRAPHQL_VALIDATION_FAILED",
          "exception": {
            "stacktrace": [
              "GraphQLError: Field \"id\" is not defined by type CursoInput.",
              "    at ObjectField (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\validation\\rules\\ValuesOfCorrectType.js:103:29)",
              "    at Object.enter (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\language\\visitor.js:324:29)",
              "    at Object.enter (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\language\\visitor.js:375:25)",
              "    at visit (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\language\\visitor.js:242:26)",
              "    at Object.validate (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\validation\\validate.js:73:24)",
              "    at validate (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:221:34)",
              "    at Object.<anonymous> (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:118:42)",
              "    at Generator.next (<anonymous>)",
              "    at fulfilled (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:5:58)",
              "    at processTicksAndRejections (internal/process/task_queues.js:94:5)"
            ]
          }
        }
      }
    ]
  }
}
```

GraphQL nos otorga también seguridad ya que no recibira valores que no esten definidos en el Input.

### <a name="lesson16"></a> Lección #16 - Variables en GraphQL
- - -
Cuando queremos trabajar con variables debemos declarar la consulta de la siguiente manera:
```sh
query obtenerCursos($input: CursoInput!) {
  obtenerCursos(input: $input) {
    titulo
  }
}
```

Una buena práctica es que la declaración del **query** o **mutation** sea con el mismo nombre del resolver (por eso vemos repetido *obtenerCursos*) aunque el nombre de afuera puede ser cualquiera. Esta es la sintáxis habitual cuando estamos trabajando con React, Vue, Angular u otra herramienta similar. Por último, las variables las definimos de esta forma:

```sh
{
  "input": {
    "tecnologia": "React"
  }
}
```

*Nota: Si estamos usando el Playground de GraphQL, esta iría en la parte inferior izquierda de la pantalla en la opción 'Query Variables'*

El resultado debe ser el siguiente:

```sh
{
  "data": {
    "obtenerCursos": [
      {
        "titulo": "React – La Guía Completa: Hooks Context Redux MERN +15 Apps"
      },
      {
        "titulo": "ReactJS Avanzado – FullStack React GraphQL y Apollo"
      }
    ]
  }
}
```

### <a name="lesson17"></a> Lección #17 - El Context en GraphQL
- - -
El context es un valor que se comparte entre todos los resolvers y lo usamos habitualmente para conocer que usuario está autenticado y cuales no por ejemplo. También nos facilita mucho a la hora de hacer consultas que únicamente le pertenezcan a un usuario en especifico, como *mis clientes*, *mis ordenes*, etc.

En la siguiente lección ya iniciaremos con el proyecto del CRM clientes.

## <a name="module04"></a> Creando una App Real en GraphQL, Apollo, Next.js, MongoDB y Mongoose

### <a name="lesson18"></a> Lección #18 - Configurando una base de datos de MongoDB
- - -
Vamos a iniciar el proyecto. Para ello eliminaremos todos los ejemplos del módulo anterior y definiremos un Query y un Resolver sencillo para que pueda arrancar el proyecto sin error. En este módulo instalaremos mongo y lo configuraremos con el proyecto.

*Nota: En el curso usan Atlas. Este es un servicio que ofrece Mongo que se encarga de gestionar tus bases de datos en la nube, ahorrandonos configuraciones, etc.*

Desde compass crearemos una base de datos llamada ``CRMGraphQL`` con una colección ``clientes`` para comprobar que todo funciona de forma correcta.

### <a name="lesson19"></a> Lección #19 - Conectando la base de datos con mongoose
- - -
Para esta lección, instalaremos dos dependencias, mongoose y dotenv:
```sh
npm i mongoose --save
npm i dotenv --save
```

Con estas dependencias estableceremos una conexión a nuestra base de datos de MongoDB.

## <a name="module05"></a> Creación de usuarios

### <a name="lesson20"></a> Lección #20 - Definiendo el modelo
- - -
Definición del modelo *Usuario*.

### <a name="lesson21"></a> Lección #21 - Creando el Type de Usuarios
- - -
Los tipos de datos que existen en GraphQL son los siguientes:
* Int: Número entero.
* Float: Número con decimales.
* String: Cadena de texto.
* ID: Número único.
* Boolean: True o False.

En esta lección vamos a definidir nuestro primer mutation que será algo sencillo que devolverá un String. Ejemplo:
```sh
mutation {
  nuevoUsuario
}
```

El resultado debe ser el siguiente:
```sh
{
  "data": {
    "nuevoUsuario": "Creando nuevo usuario"
  }
}
```

### <a name="lesson22"></a> Lección #22 - Creando el Input de Usuarios
- - -
Definición de UsuarioInput. Ejemplo en GraphQL:
```sh
mutation nuevoUsuario($input: UsuarioInput) {
  nuevoUsuario(input: $input)
}
```

Query variables:
```sh
{
  "input": {
    "nombre": "Eduardo",
    "apellido": "Márquez",
    "email": "eduardo20.3263@gmail.com",
    "password": "12345678"
  }
}
```

Resultado:
```sh
{
  "data": {
    "nuevoUsuario": "Creando..."
  }
}
```

Error si no definimos un campo obligatorio:

```sh
{
  "input": {
    "nombre": "Eduardo"
  }
}
```

```sh
{
  "error": {
    "errors": [
      {
        "message": "Variable \"$input\" got invalid value { nombre: \"Eduardo\" }; Field apellido of required type String! was not provided.",
        "locations": [
          {
            "line": 1,
            "column": 23
          }
        ],
        "extensions": {
          "code": "INTERNAL_SERVER_ERROR",
          "exception": {
            "stacktrace": [
              "GraphQLError: Variable \"$input\" got invalid value { nombre: \"Eduardo\" }; Field apellido of required type String! was not provided.",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:114:15",
              "    at coerceInputValueImpl (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\utilities\\coerceInputValue.js:99:11)",
              "    at coerceInputValue (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\utilities\\coerceInputValue.js:37:10)",
              "    at _loop (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:107:69)",
              "    at coerceVariableValues (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:119:16)",
              "    at getVariableValues (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:48:19)",
              "    at buildExecutionContext (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:184:61)",
              "    at executeImpl (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:89:20)",
              "    at Object.execute (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:64:35)",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:249:48",
              "    at Generator.next (<anonymous>)",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:8:71",
              "    at new Promise (<anonymous>)",
              "    at __awaiter (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:4:12)",
              "    at execute (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:228:20)",
              "    at Object.<anonymous> (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:176:42)"
            ]
          }
        }
      },
      {
        "message": "Variable \"$input\" got invalid value { nombre: \"Eduardo\" }; Field email of required type String! was not provided.",
        "locations": [
          {
            "line": 1,
            "column": 23
          }
        ],
        "extensions": {
          "code": "INTERNAL_SERVER_ERROR",
          "exception": {
            "stacktrace": [
              "GraphQLError: Variable \"$input\" got invalid value { nombre: \"Eduardo\" }; Field email of required type String! was not provided.",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:114:15",
              "    at coerceInputValueImpl (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\utilities\\coerceInputValue.js:99:11)",
              "    at coerceInputValue (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\utilities\\coerceInputValue.js:37:10)",
              "    at _loop (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:107:69)",
              "    at coerceVariableValues (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:119:16)",
              "    at getVariableValues (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:48:19)",
              "    at buildExecutionContext (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:184:61)",
              "    at executeImpl (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:89:20)",
              "    at Object.execute (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:64:35)",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:249:48",
              "    at Generator.next (<anonymous>)",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:8:71",
              "    at new Promise (<anonymous>)",
              "    at __awaiter (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:4:12)",
              "    at execute (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:228:20)",
              "    at Object.<anonymous> (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:176:42)"
            ]
          }
        }
      },
      {
        "message": "Variable \"$input\" got invalid value { nombre: \"Eduardo\" }; Field password of required type String! was not provided.",
        "locations": [
          {
            "line": 1,
            "column": 23
          }
        ],
        "extensions": {
          "code": "INTERNAL_SERVER_ERROR",
          "exception": {
            "stacktrace": [
              "GraphQLError: Variable \"$input\" got invalid value { nombre: \"Eduardo\" }; Field password of required type String! was not provided.",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:114:15",
              "    at coerceInputValueImpl (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\utilities\\coerceInputValue.js:99:11)",
              "    at coerceInputValue (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\utilities\\coerceInputValue.js:37:10)",
              "    at _loop (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:107:69)",
              "    at coerceVariableValues (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:119:16)",
              "    at getVariableValues (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\values.js:48:19)",
              "    at buildExecutionContext (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:184:61)",
              "    at executeImpl (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:89:20)",
              "    at Object.execute (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\graphql\\execution\\execute.js:64:35)",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:249:48",
              "    at Generator.next (<anonymous>)",
              "    at C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:8:71",
              "    at new Promise (<anonymous>)",
              "    at __awaiter (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:4:12)",
              "    at execute (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:228:20)",
              "    at Object.<anonymous> (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\node_modules\\apollo-server-core\\dist\\requestPipeline.js:176:42)"
            ]
          }
        }
      }
    ]
  }
}
```

### <a name="lesson23"></a> Lección #23 - Insertando Usuarios en la base de datos
- - -
En esta lección vamos a terminar de definir el resolver (sin hashear la contraseña). Ejemplo en GraphQL:
```sh
mutation nuevoUsuario($input: UsuarioInput) {
  nuevoUsuario(input: $input) {
    id
    nombre
    apellido
    email
  }
}
```

Query variables:
```sh
{
  "input": {
    "nombre": "Eduardo",
    "apellido": "Márquez",
    "email": "eduardo20.3263@gmail.com",
    "password": "12345678"
  }
}
```

Resultado:
```sh
{
  "data": {
    "nuevoUsuario": {
      "id": "5f01165799f12e06c065a14a",
      "nombre": "Eduardo",
      "apellido": "Márquez",
      "email": "eduardo20.3263@gmail.com"
    }
  }
}
```

Error si queremos registrar el mismo usuario:
```sh
{
  "errors": [
    {
      "message": "El usuario ya está registrado",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "nuevoUsuario"
      ],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "stacktrace": [
            "Error: El usuario ya está registrado",
            "    at nuevoUsuario (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\db\\resolvers.js:15:15)",
            "    at processTicksAndRejections (internal/process/task_queues.js:94:5)"
          ]
        }
      }
    }
  ],
  "data": {
    "nuevoUsuario": null
  }
}
```

### <a name="lesson24"></a> Lección #24 - Hasheando los Passwords
- - -
Para esta lección instalaremos la librería [bcryptjs](https://www.npmjs.com/package/bcryptjs):

```sh
npm i bcryptjs --save
```

**Importante: En la lección usan el método getSalt, en este proyecto use genSalt porque me arrojaba un error.**

## <a name="module06"></a> Autenticación de usuarios

### <a name="lesson25"></a> Lección #25 - Creando el Resolver y Mutation
- - -
En esta lección definiremos el mutation autenticarUsuario, el AutenticarInput y parte del resolver autenticarUsuario.

### <a name="lesson26"></a> Lección #26 - Creando un JSON Web Token
- - -
Para esta lección vamos a terminar de definir el resolver. Para ello debemos instalar la dependencia jsonwebtoken:

```sh
npm i jsonwebtoken --save
```

La página https://jwt.io/ sirve para decodificar las jwt. Podemos notar que en la caja **PAYLOAD** podemos ver los campos que tiene la jwt (En este caso el id, iat y exp). En una jwt podemos almacenar más valores, no solamente el id.

Ejemplo en GraphQL:
```sh
mutation autenticarUsuario($input: AutenticarInput) {
  autenticarUsuario(input: $input) {
    token
  }
}
```

Query variables:
```sh
{
  "input": {
    "email": "eduardo20.3263@gmail.com",
    "password": "12345678"
  }
}
```

Resultado:
```sh
{
  "data": {
    "autenticarUsuario": {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTM5MTY0NzAsImV4cCI6MTU5NDAwMjg3MH0.16Nmj6_P3k-sFMFgqZVgVvqWt8oUGeynyqDHkGDOhfA"
    }
  }
}
```

También podemos comprobar los errores si metemos un usuario que no existe o un password incorrecto.

*Nota: Al meter más valores en la jwt, el token irá creciendo más y más.*

### <a name="lesson27"></a> Lección #27 - Obtener el Usuario Autenticado vía JSON Web Token
- - -
En esta lección obtendremos los valores del usuario gracias a la jwt. Ejemplo en GraphQL:

```sh
query obtenerUsuario($token: String!) {
  obtenerUsuario(token: $token) {
    id
    nombre
    apellido
    email
  }
}
```

Query variables:
```sh
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTM5MTcwOTgsImV4cCI6MTU5NDAwMzQ5OH0.mVnJa37Oe_yPehU2qDIaX4mDDcq5ek8n9VWvPFqHMPY"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerUsuario": {
      "id": "5f011a814d179534bc8343ef",
      "nombre": "Eduardo",
      "apellido": "Márquez",
      "email": "eduardo20.3263@gmail.com"
    }
  }
}
```

## <a name="module07"></a> Creación de productos

### <a name="lesson28"></a> Lección #28 - Creando el modelo de Producto
- - -
Creación del modelo de Producto.

### <a name="lesson29"></a> Lección #29 - Creando Type, Input y Resolver en Schema
- - -
Definición del Type, Input y Mutation para la creación de productos.

### <a name="lesson30"></a> Lección #30 - Creando el Resolver para insertar nuevos productos
- - -
Creación del resolver para crear un *nuevoProducto*. Ejemplo en GraphQL:
```sh
mutation nuevoProducto($input: ProductoInput) {
  nuevoProducto(input: $input) {
    id
    nombre
    existencia
    precio
    creado
  }
}
```

Query variables:
```sh
{
  "input": {
    "nombre": "Monitor 27 Pulgadas",
    "existencia": 300,
    "precio": 1000
  }
}
```

Resultado:
```sh
{
  "data": {
    "nuevoProducto": {
      "id": "5f014562baf0eb16f000c045",
      "nombre": "Monitor 27 Pulgadas",
      "existencia": 300,
      "precio": 1000,
      "creado": "1593918721200"
    }
  }
}
```

Para finalizar, crearemos 3 productos en total para hacer pruebas en el siguiente módulo.
    
## <a name="module08"></a> Queries para obtener los productos y el resto de las acciones del CRUD

### <a name="lesson31"></a> Lección #31 - Obteniendo todos los productos
- - -
Creación del listado de productos. Ejemplo en GraphQL:
```sh
{
  obtenerProductos {
    id
    nombre
    existencia
    precio
    creado
  }
}
```

Resultado:
```sh
{
  "data": {
    "obtenerProductos": [
      {
        "id": "5f014562baf0eb16f000c045",
        "nombre": "Monitor 27 Pulgadas",
        "existencia": 300,
        "precio": 1000,
        "creado": "1593918721200"
      },
      {
        "id": "5f0145e4baf0eb16f000c046",
        "nombre": "Laptop i7",
        "existencia": 500,
        "precio": 2000,
        "creado": "1593918721200"
      },
      {
        "id": "5f0145fcbaf0eb16f000c047",
        "nombre": "Tablet",
        "existencia": 100,
        "precio": 500,
        "creado": "1593918721200"
      }
    ]
  }
}
```

### <a name="lesson32"></a> Lección #32 - Obteniendo un producto por su id
- - -
En esta lección crearemos el query obtenerProducto. En GraphQL:
```sh
query obtenerProducto($id: ID!) {
  obtenerProducto(id: $id) {
    id
    nombre
    existencia
    precio
    creado
  }
}
```

Query variables:
```sh
{
  "id": "5f014562baf0eb16f000c045"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerProducto": {
      "id": "5f014562baf0eb16f000c045",
      "nombre": "Monitor 27 Pulgadas",
      "existencia": 300,
      "precio": 1000,
      "creado": "1593918721200"
    }
  }
}
```

### <a name="lesson33"></a> Lección #33 - Actualizar un producto
- - -
En esta lección crearemos el mutation actualizarProducto. En GraphQL:
```sh
mutation actualizarProducto($id: ID!, $input: ProductoInput) {
  actualizarProducto(id: $id, input: $input) {
    id
    nombre
    existencia
    precio
    creado
  }
}
```

Query variables:
```sh
{
  "id": "5f014562baf0eb16f000c045",
  "input": {
    "nombre": "Monitor 27 Pulgadas",
    "existencia": 200,
    "precio": 1500
  }
}
```

```sh
{
  "data": {
    "actualizarProducto": {
      "id": "5f014562baf0eb16f000c045",
      "nombre": "Monitor 27 Pulgadas",
      "existencia": 200,
      "precio": 1500,
      "creado": "1593918721200"
    }
  }
}
```

### <a name="lesson34"></a> Lección #34 - Eliminar un producto
- - -
En esta lección crearemos el mutation eliminarProducto. En GraphQL:
```sh
mutation eliminarProducto($id: ID!) {
  eliminarProducto(id: $id)
}
```

Query variables:
```sh
{
  "id": "5f02249bd61c5f2618be6abc"
}
```

Resultado:
```sh
{
  "data": {
    "eliminarProducto": "Producto Eliminado"
  }
}
```

## <a name="module09"></a> Clientes - Creación, Listado, Actualización y Eliminación

### <a name="lesson35"></a> Lección #35 - Definiendo el Modelo de Clientes
- - -
Creación del modelo Cliente.

### <a name="lesson36"></a> Lección #36 - Definiendo Mutation, Input y Type en el Schema
- - -
Definición del mutation, input y type para nuevoCliente.

### <a name="lesson37"></a> Lección #37 - Creando el Resolver
- - -
Creación del resolver nuevoCliente. Ejemplo en GraphQL:
```sh
mutation nuevoCliente($input: ClienteInput) {
  nuevoCliente(input: $input) {
    nombre
    apellido
  }
}
```

Query variables:
```sh
{
  "input": {
    "nombre": "Kueylam",
    "apellido": "Nieto",
    "empresa": "Nomasté",
    "email": "kueylam@mail.com",
    "telefono": "0414123467"
  }
}
```

Resultado:
```sh
{
  "data": {
    "nuevoCliente": {
      "nombre": "Kueylam",
      "apellido": "Nieto"
    }
  }
}
```

En la siguiente lección veremos como asignar el vendedor usando la jwt.

### <a name="lesson38"></a> Lección #38 - Asignando Vendedor a Cliente
- - -
En esta lección, usaremos el context para asignar un vendedor al cliente usando la  jwt. Ejemplo en GraphQL:
```sh
mutation nuevoCliente($input: ClienteInput) {
  nuevoCliente(input: $input) {
    nombre
    apellido
  }
}
```

Query variables:
```sh
{
  "input": {
    "nombre": "Kueylam",
    "apellido": "Nieto",
    "empresa": "Nomasté",
    "email": "kueylam@mail.com",
    "telefono": "0414123467"
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTM5ODMwMjEsImV4cCI6MTU5NDA2OTQyMX0.glFl9U4Rx27eQj2kzemhLDQUTBqkyhTOO9pNPGat2GQ"
}
```

Resultado:
```sh
{
  "data": {
    "nuevoCliente": {
      "nombre": "Kueylam",
      "apellido": "Nieto"
    }
  }
}
```

Para esta lección crearemos dos clientes para un usuario, crearemos un nuevo usuario y le asignaremos un nuevo cliente para él.

### <a name="lesson39"></a> Lección #39 - Obtener todos los clientes
- - -
Obtener todos los cientes de todos los vendedores. Ejemplo en GraphQL:
```sh
{
  obtenerClientes {
    nombre
    empresa
    email
  }
}
```

Resultado:
```sh
{
  "data": {
    "obtenerClientes": [
      {
        "nombre": "Kueylam",
        "empresa": "Nomasté",
        "email": "kueylam@mail.com"
      },
      {
        "nombre": "Carmen",
        "empresa": "Celamar",
        "email": "carmen@mail.com"
      },
      {
        "nombre": "Luis",
        "empresa": "Gochos",
        "email": "luis@mail.com"
      }
    ]
  }
}
```

### <a name="lesson40"></a> Lección #40 - Obtener los clientes asignados a un vendedor
- - -
En esta lección, solo obtendremos los usuarios asignados a un vendedor. Para ello debemos autenticar al usuario para obtener la jwt y usarla en la consulta de GraphQL. Ejemplo:
```sh
{
  obtenerClientesVendedor {
    nombre
    apellido
    empresa
    email
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQyNTQ3ODYsImV4cCI6MTU5NDM0MTE4Nn0.g-6VcoEU0l4WU4Po0X_APOF4GX_ZYVk4BRex2GbGVkw"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerClientesVendedor": [
      {
        "id": "5f024206e55ecd3778f5d391",
        "nombre": "Kueylam",
        "apellido": "Nieto"
      },
      {
        "id": "5f02443f340130462839e136",
        "nombre": "Carmen",
        "apellido": "Rodríguez"
      }
    ]
  }
}
```

### <a name="lesson41"></a> Lección #41 - Obtener un Cliente en Específico
- - -
En esta lección, vamos a obtener el cliente de un vendedor en específico. Ejemplo en GraphQL:
```sh
query obtenerCliente($id: ID!) {
  obtenerCliente(id: $id) {
    nombre
    apellido
    empresa
    email
  }
}
```

Query variables:
```sh
{
  "id": "5f024206e55ecd3778f5d391"
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQyNTQ3ODYsImV4cCI6MTU5NDM0MTE4Nn0.g-6VcoEU0l4WU4Po0X_APOF4GX_ZYVk4BRex2GbGVkw"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerCliente": {
      "nombre": "Kueylam",
      "apellido": "Nieto",
      "empresa": "Nomasté",
      "email": "kueylam@mail.com"
    }
  }
}
```

Error si no consigue el cliente:
```sh
{
  "errors": [
    {
      "message": "Cliente no encontrado",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "obtenerCliente"
      ],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "stacktrace": [
            "Error: Cliente no encontrado",
            "    at obtenerCliente (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\db\\resolvers.js:62:15)",
            "    at processTicksAndRejections (internal/process/task_queues.js:94:5)"
          ]
        }
      }
    }
  ],
  "data": {
    "obtenerCliente": null
  }
}
```

Error de credenciales:
```sh
{
  "errors": [
    {
      "message": "No tienes las credenciales",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "obtenerCliente"
      ],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "stacktrace": [
            "Error: No tienes las credenciales",
            "    at obtenerCliente (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\db\\resolvers.js:67:15)",
            "    at processTicksAndRejections (internal/process/task_queues.js:94:5)"
          ]
        }
      }
    }
  ],
  "data": {
    "obtenerCliente": null
  }
}
```

### <a name="lesson42"></a> Lección #42 - Actualizar un cliente
- - -
En esta lección, vamos a editar el cliente de un vendedor en específico. Ejemplo en GraphQL:
```sh
mutation actualizarCliente($id: ID!, $input: ClienteInput) {
  actualizarCliente(id: $id, input: $input) {
    nombre
    email
  }
}
```

Query variables:
```sh
{
  "id": "5f024206e55ecd3778f5d391",
  "input": {
    "nombre": "Kueylam Joselyn",
    "apellido": "Nieto Nieto",
    "empresa": "Nomasté Band",
    "email": "kueylam.nomaste@gmail.com",
    "telefono": "04141234567"
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQyODk2NzYsImV4cCI6MTU5NDM3NjA3Nn0.3AWbzrHABmDozuOoLGDOWDc30py1WE7TIMCV9CRNVZk"
}
```

Resultado:
```sh
{
  "data": {
    "actualizarCliente": {
      "nombre": "Kueylam Joselyn",
      "email": "kueylam.nomaste@gmail.com"
    }
  }
}
```

### <a name="lesson43"></a> Lección #43 - Eliminar un cliente
- - -
En esta lección, vamos a eliminar el cliente de un vendedor en específico. Ejemplo en GraphQL:
```sh
mutation eliminarCliente($id: ID!) {
  eliminarCliente(id: $id)
}
```

Query variables:
```sh
{
  "id": "5f06f30b33c126360c5e3f86"
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQyODk2NzYsImV4cCI6MTU5NDM3NjA3Nn0.3AWbzrHABmDozuOoLGDOWDc30py1WE7TIMCV9CRNVZk"
}
```

Resultado:
```sh
{
  "data": {
    "eliminarCliente": "Cliente Eliminado"
  }
}
```

## <a name="module10"></a> Pedidos - Creación por medio de Mutations y Resolvers

### <a name="lesson44"></a> Lección #44 - Definiendo el Modelo de Pedidos
- - -
Creación del modelo de pedidos.

*Nota: En el curso, el modelo de Pedidos lo llamó como "ProductoSchema", yo lo llamé "PedidosSchema".*

### <a name="lesson45"></a> Lección #45 - Definiendo el Pedido en el Schema: Inputs, types y Enums
- - -
Definición del input, type y enum en el Schema. Los enums en GraphQL se definen de la siguiente manera:
```sh
enum EstadoPedido {
    PENDIENTE
    COMPLETADO
    CANCELADO
  }
```

### <a name="lesson46"></a> Lección #46 - Creando el Mutation para nuevos Pedidos
- - -
en esta lección realizaremos la creación parcial del mutation nuevoPedido. Probaremos dos cosas, si el cliente no existe y si no tenemos las credenciales. Ejemplo en GraphQL:
```sh
mutation nuevoPedido($input: PedidoInput) {
  nuevoPedido(input: $input) {
    id
    pedido {
      cantidad
    }
    cliente
  }
}
```

Query variables:
```sh
{
  "input": {
    "pedido": [
      {
        "id": "5f014562baf0eb16f000c045",
        "cantidad": 20
      }
    ],
    "total": 300,
    "cliente": "5f024206e55ecd3778f5d391",
    "estado": "PENDIENTE"
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDI0M2ZlMzQwMTMwNDYyODM5ZTEzNSIsImVtYWlsIjoiZGVuZ2VyQGdtYWlsLmNvbSIsIm5vbWJyZSI6IkRlbmdlciIsImFwZWxsaWRvIjoiWmFtb3JhIiwiaWF0IjoxNTk0NzY4MTI3LCJleHAiOjE1OTQ4NTQ1Mjd9.ViBJq1xJa8-3MJ-qguYBBIuBwlEbc6PP54PNsOs2riE"
}
```

Como nos autenticamos con un usuario que no le pertenece un cliente en específico tendremos el siguiente error:
```sh
{
  "errors": [
    {
      "message": "No tienes las credenciales",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "nuevoPedido"
      ],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "stacktrace": [
            "Error: No tienes las credenciales",
            "    at nuevoPedido (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\db\\resolvers.js:220:15)",
            "    at processTicksAndRejections (internal/process/task_queues.js:94:5)"
          ]
        }
      }
    }
  ],
  "data": {
    "nuevoPedido": null
  }
}
```

Y si ponemos un cliente que no existe:
```sh
{
  "errors": [
    {
      "message": "Ese cliente no existe",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "nuevoPedido"
      ],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "stacktrace": [
            "Error: Ese cliente no existe",
            "    at nuevoPedido (C:\\Users\\emarquez\\Documents\\courses\\udemy\\react-avanzado-next-graphql-apollo-server\\crm-graphql\\db\\resolvers.js:215:15)",
            "    at processTicksAndRejections (internal/process/task_queues.js:94:5)"
          ]
        }
      }
    }
  ],
  "data": {
    "nuevoPedido": null
  }
}
```

### <a name="lesson47"></a> Lección #47 - Revisar que haya artículos en existencia antes de Crear el Pedido
- - -
En esta lección, vamos a hacer uso de los [operadores asíncronos](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/for-await...of) para realizar las validaciones de los artículos en existencia.

### <a name="lesson48"></a> Lección #48 - Finalizando el Resolver de creación de Pedidos
- - -
En esta lección, vamos a almacenar el nuevo pedido y descontar las cantidades de los productos en Stock. Ejemplo en GraphQL:
```sh
mutation nuevoPedido($input: PedidoInput) {
  nuevoPedido(input: $input) {
    id
    cliente
    vendedor
    pedido {
      cantidad
    }
    total
    estado
    creado
  }
}
```

Query variables:
```sh
{
  "input": {
    "pedido": [
      {
        "id": "5f0145fcbaf0eb16f000c047",
        "cantidad": 5
      }
    ],
    "total": 100,
    "cliente": "5f02443f340130462839e136",
    "estado": "PENDIENTE"
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQ3NzA4NjQsImV4cCI6MTU5NDg1NzI2NH0.gcbr6doQnyKFoW1BCTVjnfr6qeJy13892Ie0h0Ya3nQ"
}
```

Resultado:
```sh
{
  "data": {
    "nuevoPedido": {
      "id": "5f0e4633e6896b4eec179776",
      "cliente": "5f02443f340130462839e136",
      "vendedor": "5f011a814d179534bc8343ef",
      "pedido": [
        {
          "cantidad": 5
        }
      ],
      "total": 100,
      "estado": "PENDIENTE",
      "creado": "1594770744236"
    }
  }
}
```

Para este punto vamos a crear 4 pedidos (2 para un mismo cliente).

## <a name="module11"></a> Pedidos - Consultar, actualizar y Eliminar

### <a name="lesson49"></a> Lección #49 - Obtener todos los Pedidos
- - -
En esta lección vamos a generar el listado de pedidos. Ejemplo en GraphQL:
```sh
{
  obtenerPedidos {
    id
    pedido {
      id
      cantidad
    }
    total
    cliente
    vendedor
    estado
    creado
  }
}
```

Resultado:
```sh
{
  "data": {
    "obtenerPedidos": [
      {
        "id": "5f0e4538e6896b4eec179773",
        "pedido": [
          {
            "id": "5f014562baf0eb16f000c045",
            "cantidad": 20
          }
        ],
        "total": 300,
        "cliente": "5f024460340130462839e137",
        "vendedor": "5f0243fe340130462839e135",
        "estado": "PENDIENTE",
        "creado": "1594770744236"
      },
      {
        "id": "5f0e4589e6896b4eec179774",
        "pedido": [
          {
            "id": "5f0145fcbaf0eb16f000c047",
            "cantidad": 40
          }
        ],
        "total": 400,
        "cliente": "5f024460340130462839e137",
        "vendedor": "5f0243fe340130462839e135",
        "estado": "PENDIENTE",
        "creado": "1594770744236"
      },
      {
        "id": "5f0e4619e6896b4eec179775",
        "pedido": [
          {
            "id": "5f0145fcbaf0eb16f000c047",
            "cantidad": 10
          }
        ],
        "total": 200,
        "cliente": "5f024206e55ecd3778f5d391",
        "vendedor": "5f011a814d179534bc8343ef",
        "estado": "PENDIENTE",
        "creado": "1594770744236"
      },
      {
        "id": "5f0e4633e6896b4eec179776",
        "pedido": [
          {
            "id": "5f0145fcbaf0eb16f000c047",
            "cantidad": 5
          }
        ],
        "total": 100,
        "cliente": "5f02443f340130462839e136",
        "vendedor": "5f011a814d179534bc8343ef",
        "estado": "PENDIENTE",
        "creado": "1594770744236"
      }
    ]
  }
}
```

### <a name="lesson50"></a> Lección #50 - Obtener los Pedidos por Vendedor
- - -
Será casi igual que la consulta anterior solo que filtraremos el vendedor usando el context. Ejemplo en GraphQL:
```sh
{
  obtenerPedidosVendedor {
    id
    pedido {
      id
      cantidad
    }
    total
    cliente
    vendedor
    estado
    creado
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQ3NzA4NjQsImV4cCI6MTU5NDg1NzI2NH0.gcbr6doQnyKFoW1BCTVjnfr6qeJy13892Ie0h0Ya3nQ"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerPedidosVendedor": [
      {
        "id": "5f0e4619e6896b4eec179775",
        "pedido": [
          {
            "id": "5f0145fcbaf0eb16f000c047",
            "cantidad": 10
          }
        ],
        "total": 200,
        "cliente": "5f024206e55ecd3778f5d391",
        "vendedor": "5f011a814d179534bc8343ef",
        "estado": "PENDIENTE",
        "creado": "1594770744236"
      },
      {
        "id": "5f0e4633e6896b4eec179776",
        "pedido": [
          {
            "id": "5f0145fcbaf0eb16f000c047",
            "cantidad": 5
          }
        ],
        "total": 100,
        "cliente": "5f02443f340130462839e136",
        "vendedor": "5f011a814d179534bc8343ef",
        "estado": "PENDIENTE",
        "creado": "1594770744236"
      }
    ]
  }
}
```

### <a name="lesson51"></a> Lección #51 - Obtener un Pedido en Específico
- - -
Obtener pedido en específico por vendedor. Ejemplo en GraphQL:
```sh
query obtenerPedido($id: ID!) {
  obtenerPedido(id: $id) {
    id
    pedido {
      id
      cantidad
    }
    total
    cliente
    vendedor
    estado
    creado
  }
}
```

Query variables:
```sh
{
  "id": "5f0e4619e6896b4eec179775"
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQ3NzA4NjQsImV4cCI6MTU5NDg1NzI2NH0.gcbr6doQnyKFoW1BCTVjnfr6qeJy13892Ie0h0Ya3nQ"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerPedido": {
      "id": "5f0e4619e6896b4eec179775",
      "pedido": [
        {
          "id": "5f0145fcbaf0eb16f000c047",
          "cantidad": 10
        }
      ],
      "total": 200,
      "cliente": "5f024206e55ecd3778f5d391",
      "vendedor": "5f011a814d179534bc8343ef",
      "estado": "PENDIENTE",
      "creado": "1594770744236"
    }
  }
}
```

### <a name="lesson52"></a> Lección #52 - Actualizar un Pedido
- - -
En esta lección vamos a actualizar un pedido. En teoría solo debemos usarlo para modificar el estado, pero actualmente sirve para modificar todos sus valores. Ejemplo en GraphQL:
```sh
mutation actualizarPedido($id: ID!, $input: PedidoInput) {
  actualizarPedido(id: $id, input: $input) {
    id
    pedido {
      id
      cantidad
    }
    total
    cliente
    vendedor
    estado
    creado
  }
}
```

Query variables:
```sh
{
  "id": "5f0e4619e6896b4eec179775",
  "input": {
    "cliente": "5f024206e55ecd3778f5d391",
    "estado": "COMPLETADO"
  }
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQ3NzA4NjQsImV4cCI6MTU5NDg1NzI2NH0.gcbr6doQnyKFoW1BCTVjnfr6qeJy13892Ie0h0Ya3nQ"
}
```

Resultado:
```sh
{
  "data": {
    "actualizarPedido": {
      "id": "5f0e4619e6896b4eec179775",
      "pedido": [
        {
          "id": "5f0145fcbaf0eb16f000c047",
          "cantidad": 10
        }
      ],
      "total": 200,
      "cliente": "5f024206e55ecd3778f5d391",
      "vendedor": "5f011a814d179534bc8343ef",
      "estado": "COMPLETADO",
      "creado": "1594770744236"
    }
  }
}
```

### <a name="lesson53"></a> Lección #53 - Eliminar un Pedido
- - -
Ejemplo en GraphQL:
```sh
mutation eliminarPedido($id: ID!) {
  eliminarPedido(id: $id)
}
```

Query variables:
```sh
{
  "id": "5f0e5d5e79e2313bc47fa7d8"
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQ3NzA4NjQsImV4cCI6MTU5NDg1NzI2NH0.gcbr6doQnyKFoW1BCTVjnfr6qeJy13892Ie0h0Ya3nQ"
}
```

Resultado:
```sh
{
  "data": {
    "eliminarPedido": "Pedido Eliminado"
  }
}
```

### <a name="lesson54"></a> Lección #54 - Obteniendo los Pedidos por su estado
- - -
Ejemplo en GraphQL:
```sh
query obtenerPedidosEstado($estado: String!) {
  obtenerPedidosEstado(estado: $estado) {
    id
    pedido {
      id
      cantidad
    }
    total
    cliente
    vendedor
    estado
    creado
  }
}
```

Query variables:
```sh
{
  "estado": "COMPLETADO"
}
```

HTTP Headers:
```sh
{
  "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDExYTgxNGQxNzk1MzRiYzgzNDNlZiIsImVtYWlsIjoiZWR1YXJkbzIwLjMyNjNAZ21haWwuY29tIiwibm9tYnJlIjoiRWR1YXJkbyIsImFwZWxsaWRvIjoiTcOhcnF1ZXoiLCJpYXQiOjE1OTQ3NzA4NjQsImV4cCI6MTU5NDg1NzI2NH0.gcbr6doQnyKFoW1BCTVjnfr6qeJy13892Ie0h0Ya3nQ"
}
```

Resultado:
```sh
{
  "data": {
    "obtenerPedidosEstado": [
      {
        "id": "5f0e4619e6896b4eec179775",
        "pedido": [
          {
            "id": "5f0145fcbaf0eb16f000c047",
            "cantidad": 10
          }
        ],
        "total": 200,
        "cliente": "5f024206e55ecd3778f5d391",
        "vendedor": "5f011a814d179534bc8343ef",
        "estado": "COMPLETADO",
        "creado": "1594770744236"
      }
    ]
  }
}
```

## <a name="module12"></a> Obtener Mejores Clientes, Mejores Vendedores y búsqueda de Productos

### <a name="lesson55"></a> Lección #55 - Obteniendo los Mejores Clientes
- - -
En esta lección, usaremos el método aggregate de mongoose que nos permite insertar código de MongoDB en las consultas a los modelos. Ejemplo en GraphQL:
```sh
{
  mejoresClientes {
    total
    cliente {
      nombre
      empresa
    }
  }
}
```

Resultado:
```sh
{
  "data": {
    "mejoresClientes": [
      {
        "total": 700,
        "cliente": [
          {
            "nombre": "Luis",
            "empresa": "Gochos"
          }
        ]
      },
      {
        "total": 200,
        "cliente": [
          {
            "nombre": "Kueylam Joselyn",
            "empresa": "Nomasté Band"
          }
        ]
      }
    ]
  }
}
```

*Nota: Los resultados solo aplica a los pedidos COMPLETADOS.*

### <a name="lesson56"></a> Lección #56 - Obteniendo los Mejores Vendedores
- - -
Hablemos un poco de la siguiente consulta de mongo:
```sh
const vendedores = await Pedido.aggregate([
  {$match: {estado: 'COMPLETADO'}},
  {$group: {
      _id: '$vendedor',
      total: {$sum: '$total'}
    }},
  {
    $lookup: {
      from: 'usuarios',
      localField: '_id',
      foreignField: '_id',
      as: 'vendedor'
    }
  },
  {
    $limit: 3
  },
  {
    $sort: {total: -1}
  }
]);
```

* **$match:** Filtra los documentos por condiciones especificas. En este ejemplo por el estado que coincida como 'COMPLETADO'.
* **$group:** Agrupa los elementos por el id especificado en la expresión (el campo debe tener el signo de $, ejemplo: $vendedor). En este ejemplo, queremos agrupar los vendedores y estamos haciendo una sumatoria al campo *total* del pedido.
* **$sum:** Debe acompañar a la expresión *$group* y nos permite realizar sumas de un campo específico. En este ejemplo el campo *total*.
* **$lookup:** Nos permite realizar una especie de Join con nuestras colecciones donde:
  * **from**: Es el nombre de la colección a la que se va a unir (usuarios).
  * **localField**: Es el nombre del campo local que se usará para la unión. En este ejemplo, como usamos un **$group** generamos un campo con el nombre **_id**.
  * **foreignField**: Es el nombre del campo foráneo que se usará para la unión. En este ejemplo, **_id**.
  * **as**: El nombre  que le daremos al campo de salida **Y ES UN ARRAY**. Como definimos en nuestro schema de GraphQL como **vendedor** debe ser el mismo nombre.
* **$limit:** Trae la cantidad de documentos con la cantidad específicada (En este caso 3).
* **$sort:** Filtra de forma ASC o DESC.

Ejemplo en GraphQL:
```sh
{
  mejoresVendedores {
    vendedor {
      nombre
      email
    }
    total
  }
}
```

Resultado:
```sh
{
  "data": {
    "mejoresVendedores": [
      {
        "vendedor": [
          {
            "nombre": "Denger",
            "email": "denger@gmail.com"
          }
        ],
        "total": 700
      },
      {
        "vendedor": [
          {
            "nombre": "Eduardo",
            "email": "eduardo20.3263@gmail.com"
          }
        ],
        "total": 200
      }
    ]
  }
}
```

### <a name="lesson57"></a> Lección #57 - Buscar Productos por nombre
- - -
Ejemplo en GraphQL:
```sh
query buscarProducto($texto: String!) {
  buscarProducto(texto: $texto) {
    nombre
    existencia
    precio
  }
}
```

Query variables:
```sh
{
  "texto": "Monitor"
}
```

Resultado:
```sh
{
  "data": {
    "buscarProducto": [
      {
        "nombre": "Monitor 17 Pulgadas",
        "existencia": 300,
        "precio": 200
      },
      {
        "nombre": "Monitor 19 Pulgadas",
        "existencia": 300,
        "precio": 300
      },
      {
        "nombre": "Monitor 27 Pulgadas",
        "existencia": 180,
        "precio": 1500
      }
    ]
  }
}
```

## <a name="module13"></a> Apollo Client - Creando un Proyecto con Next.js y React

### <a name="lesson58"></a> Lección #58 - Creando el Proyecto y SSR de Next.js
- - -
Para iniciar un proyecto con next ejecutamos el comando:
```sh
npx create-next-app crm-cliente
```

Una vez instalado, podemos probarlo ingresando a la carperta correspondiente y ejecuntando `npm run dev`. Crear páginas en next es tan sencillo como poner un nuevo componente en la carpeta page. En la escala de aprendizajes de React es la siguiente:
1. Create React App es la más básica.
2. Next es la intermedia.
3. Gatsby es el más complejo pero sirve más que todo para crear sitios web.

Next es un poco más híbrido, es decir, es tanto SSR como CSR.

En esta lección, crearemos dos pages junto con un Layout que usaremos para cada una de ellas.

### <a name="lesson59"></a> Lección #59 - Modificando el Head del Proyecto y agregando Tailwind
- - -
Se incluye el título en el Head y se usan librerías mediante cdns usando `next/head`. También se crea el sidebar de la aplicación.

### <a name="lesson60"></a> Lección #60 - Creando el Sidebar del Proyecto
- - -
En esta lección vamos a trabajar en el sidebar. Algunos de los puntos que veremos de tailwind son los siguientes:
* https://tailwindcss.com/docs/background-color/#app
* https://tailwindcss.com/docs/text-color/#app
* https://tailwindcss.com/docs/font-size/#app
* https://tailwindcss.com/docs/font-weight/#app
* https://tailwindcss.com/docs/width
* https://tailwindcss.com/docs/min-height
* https://tailwindcss.com/docs/padding/#app
* https://tailwindcss.com/docs/flex/#app

### <a name="lesson61"></a> Lección #61 - Creando el área de contenido principal
- - -
En esta lección seguiremos dandole estilos al contenido principal de la página.

### <a name="lesson62"></a> Lección #62 - Routing en Next.js y creando el Menú de Navegación
- - -
En esta lección, crearemos rutas usando `next/link`.

### <a name="lesson63"></a> Lección #63 - Destacando la página actual en Next.js y Tailwind
- - -
Para destacar la página usaremos `next/router` para obtener el pathname. Con esto destacaremos el link en el que nos encontramos.

### <a name="lesson64"></a> Lección #64 - Mostrar un diseño de forma condicional
- - -
En esta lección, vamos a establecer un Layout condicional que servirá para los pages *login* y *nuevacuenta*.

## <a name="module14"></a> Next.js y Apollo - Creando los Formularios de Login y Crear Cuenta

### <a name="lesson65"></a> Lección #65 - Creando el formulario para Login con Next y Tailwind
- - -
En esta lección crearemos un formulario usando Tailwind. No hay que sentirse abrumado con las clases ya que en la [documentación oficial](https://tailwindcss.com/components/forms/#app) hay buenos ejemplos de como podemos hacerlo.

### <a name="lesson66"></a> Lección #66 - Creando el Formulario para Crear Nueva Cuenta
- - -
En esta lección, vamos a duplicar el componente de Login en NuevaCuenta y agregaremos los campos Nombre y Apellido.

### <a name="lesson67"></a> Lección #67 - Añadiendo Formik para el manejo de los formularios
- - -
En esta lección vamos a instalar *formik* y *yup* para manejar validaciones. Las instalamos con el comando:
```sh
npm i formik yup --save
```

Usaremos formik para establecer los valores iniciales del formulario y para manejar los eventos onSubmit y onChange.

### <a name="lesson68"></a> Lección #68 - Agregando Yup para la validación
- - -
Yup lo usaremos para hacer validaciones planteando un schema. Por ejemplo, en la lección anterior vimos que nos podían enviar valores vacios. Con Yup podemos prevenir eso. También manejaremos el evento onBlur con formik.

### <a name="lesson69"></a> Lección #69 - Agregando el Resto de las validaciones
- - -
En esta lección agregaremos el resto de las validaciones a los campos apellido, email y password.

### <a name="lesson70"></a> Lección #70 - Configurando Apollo Client
- - -
Para esta lección usaremos las dependencias `@apollo/client` y `node-fetch`. Las instalamos con el comando:
```sh
npm i @apollo/client node-fetch --save
```

Además, debemos reescribir el archivo principal de Next para que tome la configuración de Apollo Client. En CRA ese archivo sería `index.js`, pero en Next no existe dicho archivo. El archivo de Next se encuentra oculto pero nosotros podemos sobreescribirlo si creamos un page llamado _app.js y es aquí donde agregaremos la configuración de Apollo Client para que se propague en todos los componentes.

### <a name="lesson71"></a> Lección #71 - Instalando y Configurando Apollo Dev Tools
- - -
Seguiremos configurando nuestro archivo _app.js. Como nuestro proyecto usa graphql debemos instalar dicha dependencia:
```sh
npm i graphql --save
```

Ademas, instalaremos la extensión *Apollo Client Developer Tools* en nuestro navegador para hacer consultas de GraphQL desde este.

### <a name="lesson72"></a> Lección #72 - Realizando una consulta de prueba para confirmar que todo funciona
- - -
En esta lección usaremos el hook useQuery para realizar consultas y gql para escribir nuestras consultas. El hook useQuery retorna los siguientes valores:
* data: Que es la data de la consulta.
* loading: Un boolean que indica si la consulta está cargando o no y es muy útil para establecer un spinner o un loader por ejemplo.
* error: Que nos indica si existió un error en la consulta. Sino existe el error, será undefined.

Haremos una consulta para obtenerProductos como ejemplo.

### <a name="lesson73"></a> Lección #73 - Creando un Usuario desde el formulario
- - -
En esta lección usaremos el hook useMutation para crear un nuevo usuario desde el formulario. Con los bloques try catch podemos atrapar los errores del servidor, como por ejemplo: "El usuario ya está registrado".

### <a name="lesson74"></a> Lección #74 - Mostrando un mensaje si el usuario ya está registrado
- - -
En esta lección usaremos los estados para indicar un mensaje de error al usuario si ya está registrado.

### <a name="lesson75"></a> Lección #75 - Finalizando la creación de Usuarios
- - -
En esta lección, mostraremos un mensaje cuando el usuario se registre de forma exitosa y lo redirigiremos al login.

### <a name="lesson76"></a> Lección #76 - Trabajando con el Login y Formik
- - -
Validaciones con Formik y Yup en el formulario de Login.

### <a name="lesson77"></a> Lección #77 - Enviando la petición al servidor con useMutation
- - -
Autenticación del usuario.

### <a name="lesson78"></a> Lección #78 - Almacenando el Token en Storage
- - -
En esta lección, almacenaremos el token en localStorage y redirigiremos a la página principal.